# FFtypefx

This script will convert two text strings into aegi subs format (ass) with typewriting effect for use with ffmpeg and encode it onto the first 8 seconds (four seconds per string) of a video, cropping it to 1080x1080px - formatting it for Instagram (8 second loop).

Requires ffmpeg installed.
