#/bin/bash

black=$(tput setaf 0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)  
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)   
white=$(tput setaf 7)  

read -p "${green}This script will convert ${red}two text strings ${green}into aegi subs format ${blue}(ass) ${green}with typewriting effect for use with ${red}ffmpeg ${green}and encode it onto the first 8 seconds ${red}(four seconds per string)${green} of a video, ${blue}cropping it to 1080x1080px${green} - formatting it for ${blue}Instagram ${green}(8 second loop). ${red}Press Enter to continue."

read -p "${yellow}Enter first string ${red}(~55characters)${yellow}: " string1
read -p "${cyan}Enter second string: ${red}(~55characters)${cyan}: " string2

subs='
[Script Info]
PlayResX: 1080  
PlayResY: 1080

[V4+ Styles]
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding
Style: style1,Monospace,75,&H00FFFFFF,&HFF0000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,0,0,4,10,10,10,1
[Events]
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text
Dialogue: 0,0:00:01.10,0:00:04.00,style1,,0,0,0,,{\pos(30,187.5)}'
subs2='
Dialogue: 0,0:00:05.10,1:00:08.00,style1,,0,0,0,,{\pos(30,187.5)}'

string1=$(echo $string1 | sed 's/\([^ ]\)/{\\k2\}\1{\\k2\}/g')
string2=$(echo $string2 | sed 's/\([^ ]\)/{\\k2\}\1{\\k2\}/g')

printf '%s' "$subs" "$string1" "$subs2" "$string2" > subs.ass

read -p "${magenta}Saved subs to subs.ass, ${red}press Enter to start ffmpeg."

ls -al

read -p "${cyan}Provide input video filename (${red}no spaces allowed!${cyan}): " input

ffmpeg -i "$input" -vf "crop=w=1080:h=1080,subtitles=subs.ass:force_style='BackColour=&H80000000,BorderStyle=4'" -an -crf 17 -t 8 "$input"-insta-tw.mp4
